import React, { useState } from "react";
import SearchForm from "./Components/SearchForm";
import SearchResults from "./Components/SearchResults";
import AddUserForm from "./Components/AddUserForm";
import { AddUser, SearchUsers } from "./ServerInterface/FormRequests";

const App = () => 
{
    const [searchResults, setSearchResults] = useState([]);

    return (
        <div className="app">
            <div className="form-container" >
                <SearchForm resultsCallback={setSearchResults} search={SearchUsers}></SearchForm>
                <AddUserForm add={AddUser}></AddUserForm>
            </div>
            <br />
            <div>
                <SearchResults results={searchResults}></SearchResults>
            </div>
        </div>
    );
};

export default App;
