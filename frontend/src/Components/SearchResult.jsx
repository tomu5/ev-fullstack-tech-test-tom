import React from "react";

export default function ({resultData}) 
{

    function getDisplayDate(dateStr)
    {
        const date = new Date(dateStr);
        return date.toLocaleDateString();
    }
    return (
        <div className="search-result">
            <table>
                <tbody>
                    <tr>
                        <td>ID: {resultData._id}</td><td>Company: {resultData.company}</td>
                    </tr>
                    <tr>
                        <td>Name: {resultData.name}</td><td>User Added: {getDisplayDate(resultData.createdAt)}</td>
                    </tr>
                    <tr>
                        <td>Email: {resultData.email}</td><td>User Updated: {getDisplayDate(resultData.updatedAt)}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}