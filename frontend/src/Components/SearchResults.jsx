import React from "react";
import SearchResult from "./SearchResult";

export default function (props) 
{
    return (
        <div className="results-container">
            {props.results.map((result, resultIdx) => (
                <SearchResult key={resultIdx} resultData={result}></SearchResult>
            ))}
        </div>
    );
}