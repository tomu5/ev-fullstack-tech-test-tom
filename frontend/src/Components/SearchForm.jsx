import React, {useState} from "react";

export default function (props) 
{
    const [id, setId] = useState("");
    const [name, setName] = useState("");
    const [company, setCompany] = useState("");
    const [email, setEmail] = useState("");
 
    const searchUser = () => 
    {
        props.search({
            "id": id,
            "name": name,
            "company": company,
            "email": email
        }, 
        props.resultsCallback);
        // reset form
        setId("");
        setName("");
        setCompany("");
        setEmail("");
    }
    return (
        <form className="search-form" action="javascript:void(0);">
            <label htmlFor="ID">ID <input type="text" name="ID" onChange={(e)=>setId(e.target.value)} value={id}></input></label>
            <label htmlFor="Name">Name <input type="text" name="Name" onChange={(e)=>setName(e.target.value)} value={name}></input></label>
            <label htmlFor="Email">Email <input type="text" name="Email" onChange={(e)=>setEmail(e.target.value)} value={email}></input></label>
            <label htmlFor="Company">Company <input type="text" name="Company" onChange={(e)=>setCompany(e.target.value)} value={company}></input></label>
           
            <button className="search-button" title="Search for user" onClick={searchUser}>Search</button>
        </form>
    );
}