import React, { useState } from "react";

export default function (props) 
{
    const [name, setName] = useState("");
    const [company, setCompany] = useState("");
    const [email, setEmail] = useState("");
    
    const addUser = () => 
    {
        props.add({
            "name": name,
            "email": email,
            "company": company
        });
        // reset form
        setCompany("");
        setName("");
        setEmail("");
    }
    return (
        <form className="adduser-form" action="javascript:void(0);">
            <label htmlFor="Name">Name <input type="text" name="Name" onChange={(e)=>setName(e.target.value)} value={name}></input></label>
            <label htmlFor="Email">Email <input type="text" name="Email" onChange={(e)=>setEmail(e.target.value)} value={email}></input></label>
            <label htmlFor="Company">Company <input type="text" name="Company" onChange={(e)=>setCompany(e.target.value)} value={company}></input></label>
            <button className="add-button" title="Add user" onClick={addUser}>Add</button>
        </form>
    );
}