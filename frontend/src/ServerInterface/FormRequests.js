import axios from "axios";

const url = "http://localhost:3001/users";
const RequestType = {
    GET: "get",
    POST: "post"
}
const SendRequest = (type, params, successCallback) =>
{
    const axiosReq = type == RequestType.POST ? axios.post : axios.get;

    axiosReq(url, type == RequestType.POST ? params : {params})
        .then(response => 
            {
                if (typeof successCallback == "function")
                    successCallback(response.data);
            }
        )
        .catch(e => console.error(e));
}

const SearchUsers = (searchParams = null, successCallback = null) => 
{
    SendRequest(RequestType.GET, searchParams, successCallback);
};

const AddUser = (searchParams, successCallback = null) => 
{
    SendRequest(RequestType.POST, searchParams, successCallback);
}
export { AddUser, SearchUsers }