# Introduction
API to administer a collection of users

# Overview
Accessed on http://localhost:3001 by default
The request type dictates the operation to be done

# Authentication
None

# Error Codes
Request ok - 200
Request ok, resource created - 201
Request ok, no content response - 204
Bad request - 400
Requested resource not found - 404


# Rate limit
None