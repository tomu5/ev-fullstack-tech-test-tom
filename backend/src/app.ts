import cors from "cors";
import mongoose from "mongoose";
import express, {Express, json} from 'express';
import userRouter from "./Routes/UserRouter";

require("dotenv").config();
const app: Express = express();

app.use(cors());
app.use(json());
app.use(userRouter);

const PORT: string|number = process.env.PORT || 3001;
const URI: string = 
	`mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASS}
	@${process.env.MONGO_CLUSTER}/${process.env.MONGO_DB_NAME}
	?retryWrites=true&w=majority`;

try 
{
	mongoose.connect(URI)
		.then(() => app.listen(PORT, () => console.log(`Server listening on localhost:${PORT}`)))
		.catch(err => {throw err});
}
catch (e)
{
	console.error(e);
}