import { Document, model, Schema } from "mongoose";

export interface IUser extends Document
{
	name: string;
	age: number;
	hasFacebook: boolean;
}

const userSchema: Schema = new Schema(
	{
		name: {
			type: String,
			required: true,
		},
		email: {
			type: String,
			required: true,
		},
		company: {
			type: String,
			required: true,
		}
	}, 
	{ timestamps: true }
);

const User = model<IUser>("User", userSchema);
export default User;