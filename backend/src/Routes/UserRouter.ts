import {Router} from "express";
import {getUsers, addUser} from "../Controllers/UsersController"

const userRouter: Router = Router();
userRouter.get("/users", getUsers);
userRouter.post("/users", addUser);

export default userRouter