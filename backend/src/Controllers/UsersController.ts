import { Response, Request } from "express";
import User, {IUser} from "../Models/User";

enum httpStatus
{
	OK = 200,
	OK_CREATED = 201,
	OK_NO_CONTENT = 204,
	BAD_REQUEST = 400,
	NOT_FOUND = 404
}

interface UserSearchParams 
{
	_id?: string,
	name?: string,
	email?: string,
	company?: string,
}

const getUsers = async (req: Request, res: Response): Promise<void> =>
{
	try
	{
		const queryParams: UserSearchParams = {};
		const reqParams = req.query;
		
		if (reqParams.id?.length)
			queryParams._id = reqParams.id.toString();
		
		if (reqParams.name?.length)
			queryParams.name = reqParams.name.toString();
	
		if (reqParams.email?.length)
			queryParams.email = reqParams.email.toString();
	
		if (reqParams.company?.length)
			queryParams.company = reqParams.company.toString();
	
		const query = await User.find(queryParams);
		res.status(httpStatus.OK).json(query);
	}
	catch (e)
	{
		res.status(httpStatus.NOT_FOUND).send();
	}
}

const addUser = async (req: Request, res: Response): Promise<void> =>
{
	const body = req.body;
	try
	{ 
		if (!body || !body.name || !body.email || !body.company)
			return replyBadRequest(res);

		const user: IUser = new User(
		{
			name: body.name,
			email: body.email,
			company: body.company
		});
		
		await user.save()
		.then(() =>
		{
			res.status(httpStatus.OK_CREATED).send();
		})
		.catch(err => console.log(err));
	}
	catch (e)
	{
		replyBadRequest(res);
	}
}

function replyBadRequest(res: Response): void
{
	res.status(httpStatus.BAD_REQUEST).send();	
}

export { getUsers, addUser }